package Controle;

import static Controle.Token.*;
%%
%class Lexer
%type Token
L = [a-zA-Z_]
D = [0-9]
WHITE=[ \t\r]

%{
public String lexeme;
%}
%%
{WHITE} {/*Ignore*/}

/* Pular linha */
("\n")    {lexeme = yytext(); return LINHA;}

/************************Analise Sintatico Pilha***************************************/
/* Programa */
("<programa>")    {lexeme = yytext(); return PROGRAMA;}

/*Lista de funções*/
("<lista_de_funcoes>") {lexeme = yytext(); return LISTFUNCOES;}

/*ID Escopo*/
("<id>")    {lexeme = yytext(); return ID_ESC;}

/*Função*/
("<funcao>") {lexeme=yytext(); return FUNCAO_ESC;}

/*Função*/
("<fun>") {lexeme=yytext(); return FUN;}

/*Corpo da Função*/
("<corpo>") {lexeme = yytext(); return CORPO;}

/*Parametros*/
("<parametros>") {lexeme = yytext(); return PARAMETROS;}

/*Parametros*/
("<par>") {lexeme = yytext(); return PAR;}

/*Expressões*/
("<exp>") {lexeme = yytext(); return EXP;}

/*Condições*/
("<cond>") {lexeme = yytext(); return COND;}

/*Numero Escopo*/
("<num>") {lexeme = yytext(); return NUM;}
/**************************Comum para os dois****************************************/
/*Delimitador Esquedo*/
( "(" )    {lexeme = yytext(); return DELIMESQ;}

/*Delimitador direito*/
( ")" )    {lexeme = yytext(); return DELIMDIR;}

/*Função*/
("defun")  {lexeme = yytext(); return FUNCAO;}

/*Operador +*/
("+") {lexeme = yytext(); return MAIS;}

/*Operador -*/
("-") {lexeme = yytext(); return MENOS;}

/*Operador vezes */
("*") {lexeme = yytext(); return VEZES;}

/*Operador div */
("/") {lexeme = yytext(); return DIV;}

/*Operador OR*/
("or") {lexeme = yytext(); return OR;}

/*Operador AND*/
("and") {lexeme = yytext(); return AND;}

/*Operador Negação*/
("not") {lexeme = yytext(); return NOT;}

/*Operador Equivalente*/
("eq") {lexeme = yytext(); return IGUAL;}

/*Operador Diferente*/
("neq") {lexeme = yytext(); return DIF;}

/*Teste condicional*/
("if")  {lexeme = yytext(); return TESTE_COND;}

/*Operador maior*/
("gt")  {lexeme = yytext(); return MAIOR;}

/*Operador menor*/
("lt")  {lexeme = yytext(); return MENOR;}

/*Operador maior ou igual*/
("geq")  {lexeme = yytext(); return MAIOR_IGUAL;}

/*Operador menor ou igual*/
("leq")  {lexeme = yytext(); return MENOR_IGUAL;}

/*ID*/
{L}({L}|{D})* {lexeme=yytext(); return ID;}

/*Numero*/
{D}{D}* {lexeme = yytext(); return NUMERO;}

/*Erro de sintaxe*/
. {return ERROR;}