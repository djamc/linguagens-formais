/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

/**
 *
 * @author casa
 */
public enum Token {
    LINHA,
    DELIMESQ,
    DELIMDIR,
    FUNCAO,
    ID,
    ID_ESC,
    LISTFUNCOES,
    TESTE_COND,
    NUMERO,
    NUM,
    ERROR,
    PROGRAMA,
    FUNCAO_ESC,
    FUN,
    CORPO,
    PARAMETROS,
    PAR,
    EXP,
    COND,
    MAIS,
    MENOS,
    VEZES,
    DIV,
    OR,
    AND,
    NOT,
    IGUAL,
    DIF,
    MAIOR,
    MENOR,
    MAIOR_IGUAL,
    MENOR_IGUAL,
    FIM_ARQUIVO
}
