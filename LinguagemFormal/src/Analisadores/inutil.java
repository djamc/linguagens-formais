/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisadores;

import Controle.Token;
import Interface.Tela;
import java.io.StringReader;
import java.io.IOException;

/**
 *
 * @author djand
 */
public class inutil {
    
    public String al(String arquivo) throws IOException{
        
        int linha = 1;
        String resultados = "";
        Lexer lexer = new Lexer(new StringReader(arquivo));
        resultados += "Linha " + linha + ":\n";
        while(true){
            Token token = lexer.yylex();
            if(token == null)       break;
            switch(token){
                case LINHA:
                    linha++;
                    resultados += "Linha " + linha + ":\n";
                    break;
                case DELIMESQ:
                    resultados += "Delimitador a esquerda, " + lexer.lexeme + "\n";
                    break;
                case DELIMDIR:
                    resultados += "Delimitador a direita, " + lexer.lexeme + "\n";
                    break;
                case FUNCAO:
                    resultados += "Função, " + lexer.lexeme + "\n";
                    break;
                case ID:
                    resultados += "Nome da função, " + lexer.lexeme + "\n";
                    break;
                case TESTE_COND:
                    resultados += "Teste condição, " + lexer.lexeme + "\n";
                    break;
                case MAIS:
                    resultados += "Operador Unitario, " + lexer.lexeme + "\n";
                    break;
                case MENOS:
                    resultados += "Operador Unitario, " + lexer.lexeme + "\n";
                    break;
                case VEZES:
                    resultados += "Operador Unitario, " + lexer.lexeme + "\n";
                    break;
                case DIV:
                    resultados += "Operador Unitario, " + lexer.lexeme + "\n";
                    break;
                case OR:
                    resultados += "Operador Condicional, " + lexer.lexeme + "\n";
                    break;
                case AND:
                    resultados += "Operador Condicional, " + lexer.lexeme + "\n";
                    break;
                case NOT:
                    resultados += "Operador Condicional, " + lexer.lexeme + "\n";
                    break;
                case IGUAL:
                    resultados += "Operador Condicional, " + lexer.lexeme + "\n";
                    break;
                case NUMERO:
                    resultados += "Numero, " + lexer.lexeme + "\n";
                    break;
                case ERROR:
                    resultados += "Erro de sistaxe, " + lexer.lexeme + "\n";
                    break;
            }
        }
        
        return resultados;
    }
    
}
