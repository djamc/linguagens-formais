/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisadores;

/**
 *
 * @author djand
 */
public class Resultados {
    
    private String resulLexico;
    private String resulSintaEnt;
    private String resulSintaPilha;

    public String getResulLexico() {
        return resulLexico;
    }

    public void setResulLexico(String resulLexico) {
        this.resulLexico = resulLexico;
    }

    public String getResulSintaEnt() {
        return resulSintaEnt;
    }

    public void setResulSintaEnt(String resulSintaEnt) {
        this.resulSintaEnt = resulSintaEnt;
    }

    public String getResulSintaPilha() {
        return resulSintaPilha;
    }

    public void setResulSintaPilha(String resulSintaPilha) {
        this.resulSintaPilha = resulSintaPilha;
    }
    
}
