/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisadores;

import Controle.Token;
import static Controle.Token.*;
import java.io.IOException;
import java.io.StringReader;
import javax.swing.JOptionPane;

/**
 *
 * @author djand
 */
public class Analisadores {
    
    private int contParentesesEnt;
    private int contParentesesPilha;
    private int linha;
    private String pilha;
    private String entrada;
    private String resulLexico;
    private String resulSintaEnt;
    private String resulSintaPilha;
    private Resultados resul;
    private Lexer lexerEnt;
    private Lexer lexerPilha;
    private Token tokenEnt;
    private Token tokenPilha;
    private boolean erro;
    
    public Resultados Analisar(String arquivo) throws IOException{
        this.entrada = arquivo;
        this.pilha = "<programa>";
        this.contParentesesEnt = this.contParentesesPilha = 0;
        this.linha = 1;
        this.resulLexico = "Linha " + linha + ":\n";
        this.resulSintaEnt = "";
        this.resulSintaPilha = "";
        this.resulSintaPilha += this.pilha + "\n";
        this.erro = false;
        resul = new Resultados();
        String temp;
        lexerEnt = new Lexer(new StringReader(arquivo));
        while(true){
            tokenEnt = lexerEnt.yylex();
            if(tokenEnt == null)       break;
            if(this.erro)       break;
            switch(tokenEnt){
                case LINHA:
                    linha++;
                    resulLexico += "Linha " + linha + ":\n";
                    break;
                case DELIMESQ:
                    resulLexico += "Delimitador a esquerda, " + lexerEnt.lexeme + "\n";
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    this.contParentesesEnt++;
                    this.delimEsq();
                    break;
                case DELIMDIR:
                    resulLexico += "Delimitador a direita, " + lexerEnt.lexeme + "\n";
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    this.delimDir();
                    break;
                case FUNCAO:
                    resulLexico += "Função, " + lexerEnt.lexeme + "\n";
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    this.defun();
                    break;
                case ID:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    this.id();
                    break;
                case TESTE_COND:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Teste condição, " + lexerEnt.lexeme + "\n";
                    this.testCond();
                    break;
                case MAIS:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Unitario, " + lexerEnt.lexeme + "\n";
                    this.mais();
                    break;
                case MENOS:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Unitario, " + lexerEnt.lexeme + "\n";
                    this.menos();
                    break;
                case VEZES:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Unitario, " + lexerEnt.lexeme + "\n";
                    this.vezes();
                    break;
                case DIV:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Unitario, " + lexerEnt.lexeme + "\n";
                    this.div();
                    break;
                case OR:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional, " + lexerEnt.lexeme + "\n";
                    this.or();
                    break;
                case AND:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional, " + lexerEnt.lexeme + "\n";
                    this.and();
                    break;
                case NOT:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional, " + lexerEnt.lexeme + "\n";
                    this.not();
                    break;
                case IGUAL:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional de igualdada, " + lexerEnt.lexeme + "\n";
                    this.igual();
                    break;
                case DIF:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional de diferença, " + lexerEnt.lexeme + "\n";
                    this.dif();
                    break;
                case MAIOR:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional de maior, " + lexerEnt.lexeme + "\n";
                    this.maior();
                    break;
                case MENOR:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional de menor, " + lexerEnt.lexeme + "\n";
                    this.menor();
                    break;
                case MAIOR_IGUAL:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional de maior ou igual, " + lexerEnt.lexeme + "\n";
                    this.maiorIgual();
                    break;
                case MENOR_IGUAL:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Operador Condicional de menor ou igual, " + lexerEnt.lexeme + "\n";
                    this.menorIgual();
                    break;
                case NUMERO:
                    this.resulSintaEnt += lexerEnt.lexeme + "\n";
                    resulLexico += "Numero, " + lexerEnt.lexeme + "\n";
                    this.num();
                    break;
                case FIM_ARQUIVO:
                    resulLexico += "Fim do arquivo\n";
                case ERROR:
                    resulLexico += "Erro de sistaxe, " + lexerEnt.lexeme + "\n";
                    break;
            }
        }
        if(this.pilha != "" && this.erro == false)    this.finalPilha();
        if(this.contParentesesEnt != 0 || this.contParentesesPilha != 0){
            this.resulLexico += "Erro de sintaxe\n";
        }
        resul.setResulLexico(resulLexico);
        resul.setResulSintaEnt(resulSintaEnt);
        resul.setResulSintaPilha(resulSintaPilha);
        return resul;
    }
    
    public void delimEsq() throws IOException{
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case DELIMESQ:
                    this.contParentesesPilha++;
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    if(this.contParentesesEnt != this.contParentesesPilha)  System.out.println("erro de parenteses");
                    return;
                case DELIMDIR:
                    this.contParentesesPilha--;
                    System.out.println("Erros de sintaxe");
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    break;
                case CORPO:
                    return;
                case COND:
                    return;
                case EXP:
                    return;
                case FUN:
                    this.pilha = "<lista_de_funções>";
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case PROGRAMA:
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    public void delimDir() throws IOException{
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case FUN:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    break;
                case PAR:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    return;
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    return;
                case DELIMDIR:
                    this.contParentesesPilha--;
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    if(this.contParentesesEnt != this.contParentesesPilha)  System.out.println("erro de parenteses");
                    return;
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void defun() throws IOException{
        String tmp;
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            System.out.println(this.resulSintaPilha);
            switch(tokenPilha){
                case PROGRAMA:
                    this.pilha = "<lista_de_funcoes>";
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case LISTFUNCOES:
                    this.pilha = "<funcao><fun>";
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case FUNCAO_ESC:
                    this.pilha = this.pilha.substring (8, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(defun<id>(<parametros>)<corpo>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.contParentesesPilha++;
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    break;
                case DELIMDIR:
                    this.contParentesesPilha--;
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    break;
                case FUNCAO:
                    tokenEnt = lexerEnt.yylex();
                    tokenPilha = lexerPilha.yylex();
                    this.resulSintaPilha += this.pilha + "\n";
                    if(tokenEnt == ID && tokenPilha == ID_ESC){
                        resulLexico += "Nome da função, " + lexerEnt.lexeme + "\n";
                        this.pilha = this.pilha.substring (9, this.pilha.length());
                        return;
                    }
                    break;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void id() throws IOException{
        String tmp = "";
        boolean var = false;
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case PARAMETROS:
                    this.pilha = this.pilha.substring (12, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<id><par>"+tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    var = true;
                    break;
                case PAR:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<id>"+tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    var = true;
                    break;
                case ID_ESC:
                    this.pilha = this.pilha.substring (4, this.pilha.length());
                    if(var){
                        resulLexico += "Nome da variavel, " + lexerEnt.lexeme + "\n";
                        var = false;
                    }else{
                        resulLexico += "Nome da função, " + lexerEnt.lexeme + "\n";
                    }
                    return;
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(<id><exp>)"+tmp;
                    resulLexico += "Nome da variavel, " + lexerEnt.lexeme + "\n";
                    var = true;
                    break;
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<id>"+tmp;
                    resulLexico += "Nome da variavel, " + lexerEnt.lexeme + "\n";
                    var = true;
                    break;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void testCond() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(if<cond><corpo><corpo>)" + tmp;
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case TESTE_COND:
                    this.pilha = this.pilha.substring (2, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void mais() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<exp>" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(+<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case MAIS:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void menos() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<exp>" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(-<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case MENOS:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void vezes() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<exp>" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(*<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case VEZES:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    
    
    private void div() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<exp>" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(/<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DIV:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void or() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(or<cond><cond>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case OR:
                    this.pilha = this.pilha.substring (2, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void and() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(and<cond><cond>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case AND:
                    this.pilha = this.pilha.substring (3, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void not() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(not<cond>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case NOT:
                    this.pilha = this.pilha.substring (3, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void igual() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(eq<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case IGUAL:
                    this.pilha = this.pilha.substring (2, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void dif() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(neq<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case DIF:
                    this.pilha = this.pilha.substring (3, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void maior() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(gt<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case MAIOR:
                    this.pilha = this.pilha.substring (2, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void menor() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(lt<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case MENOR:
                    this.pilha = this.pilha.substring (2, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void maiorIgual() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(geq<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case MAIOR_IGUAL:
                    this.pilha = this.pilha.substring (3, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void menorIgual() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case COND:
                    this.pilha = this.pilha.substring (6, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "(leq<exp><exp>)" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case MENOR_IGUAL:
                    this.pilha = this.pilha.substring (3, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void num() throws IOException{
        String tmp = "";
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case EXP:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    tmp = this.pilha;
                    this.pilha = "<num>" + tmp;
                    this.resulSintaPilha += this.pilha + "\n";
                    break;
                case DELIMESQ:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha++;
                    break;
                case DELIMDIR:
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    this.contParentesesPilha--;
                    break;
                case NUM:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
        }
    }
    
    private void finalPilha() throws IOException{
        while(true){
            lexerPilha = new Lexer(new StringReader(this.pilha));
            tokenPilha = lexerPilha.yylex();
            if(tokenPilha == null)  break;
            this.resulSintaPilha += this.pilha + "\n";
            switch(tokenPilha){
                case FUN:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    break;
                case PAR:
                    this.pilha = this.pilha.substring (5, this.pilha.length());
                    return;
                case CORPO:
                    this.pilha = this.pilha.substring (7, this.pilha.length());
                    return;
                case DELIMDIR:
                    this.contParentesesPilha--;
                    this.pilha = this.pilha.substring (1, this.pilha.length());
                    if(this.contParentesesEnt != this.contParentesesPilha)  System.out.println("erro de parenteses");
                    return;
                default:
                    this.erro = true;
                    JOptionPane.showMessageDialog(null, "Erro de sintaxe na linha " + this.linha, "Erro de sintaxe", JOptionPane.ERROR_MESSAGE);
                    return;
            }
            if(this.pilha == "") break;
        }
    }
}